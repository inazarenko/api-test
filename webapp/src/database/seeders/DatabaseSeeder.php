<?php

namespace Database\Seeders;

use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $user = new User();
        $user->login = 'admin';
        $user->email = 'admin@example.com';
        $user->password = Hash::make('admin');
        $user->abilities = ['*'];
        $user->save();
        $user = new User();
        $user->login = 'user';
        $user->email = 'user@example.com';
        $user->password = Hash::make('user');
        $user->abilities = ['user'];
        $user->save();
        Post::factory()->count(50)->create();
        Post::whereId(1)->update(['is_published'=>false]);
    }
}
