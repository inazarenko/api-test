<?php

namespace Database\Factories;


use App\Models\Post;
use Illuminate\Database\Eloquent\Factories\Factory;

class PostFactory extends Factory
{
    private static $postIndex=0;
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Post::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'author_id' => (self::$postIndex++ % 2)+1,
            'caption' => $this->faker->text(64),
            'body' => $this->faker->text(),
            'is_published' => self::$postIndex % 10 !== 0,
            'created_at' => now(),
            'updated_at' => now(),
        ];
    }
}
