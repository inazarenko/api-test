<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('auth')->group(function () {
    Route::post('/register', [AuthController::class, 'register']);
    Route::post('/login', [AuthController::class, 'login']);
    Route::get('/reset-password', [AuthController::class, 'requestResetCode']);
    Route::post('/reset-password', [AuthController::class, 'resetPassword']);
    Route::post('/refresh', [AuthController::class, 'refreshTokens']);
});

Route::middleware(['auth:sanctum'])->group(function () {
    Route::post('/auth/logout', [AuthController::class, 'logout']);
    Route::patch('/user/{id}', [UserController::class, 'updateUser'])
        ->where('id','^(me|[0-9]+)$');
    Route::delete('/user/{id}', [UserController::class, 'deleteUser'])
        ->where('id','^(me|[0-9]+)$');
    Route::get('/user/{id}', [UserController::class, 'readUser'])
        ->where('id','^(me|[0-9]+)$');
    Route::get('/user/find', [UserController::class, 'findUser']);
    Route::apiResource('posts', PostController::class);
});
