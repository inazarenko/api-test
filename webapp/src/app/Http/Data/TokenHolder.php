<?php

namespace App\Http\Data;

class TokenHolder
{
    public string $authToken;
    public string $refreshToken;

    /**
     * @param string $authToken
     * @param string $refreshToken
     */
    public function __construct(string $authToken, string $refreshToken)
    {
        $this->authToken = $authToken;
        $this->refreshToken = $refreshToken;
    }
}
