<?php

namespace App\Http\Controllers;

use App\Models\ApiPasswordResets;
use App\Models\User;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class AuthController extends APIController
{

    public function register(Request $request): JsonResponse
    {
        $data = $request->validate( [
            'login' => ['required', 'string', 'max:191', 'unique:users'],
            'email' => ['required', 'string', 'email', 'max:191', 'unique:users'],
            'password' => ['required', 'string', 'min:8'],
        ]);
        $data['password'] = Hash::make($data['password']);
        $data['abilities'] = config('auth.default_abilities');
        /** @var User $user */
        $user = User::create($data);
        return $this->makeJsonResponse($user->regenerateTokens());
    }

    /**
     * @throws AuthenticationException
     */
    public function login(Request $request): JsonResponse
    {
        $data = $request->validate( [
            'email' => ['required', 'string', 'email'],
            'password' => ['required', 'string'],
        ]);
        $data['abilities'] = ['user', 'viewer'];
        /** @var User $user */
        $user = User::where('email', $data['email'])->first();
        if ($user && Hash::check($data['password'], $user->password)) {
            return $this->makeJsonResponse($user->regenerateTokens());
        }
        throw new AuthenticationException();
    }

    public function requestResetCode(Request $request): JsonResponse
    {
        $data = $request->validate( [
            'email' => ['required', 'string', 'email', 'max:191'],
        ]);
        /** @var User $user */
        $user = User::where('email', $data['email'])->first();
        if ($user) {
            $code = Str::random(config('auth.reset_code_size'));
            ApiPasswordResets::create([
                'user_id' => $user->id,
                'signature' => make_signature([$user->email, $code]),
                'created_at' => now()
            ]);
            $user->sendPasswordResetNotification($code);
        }
        return $this->makeJsonResponse(null);
    }

    /**
     * @throws AuthenticationException
     */
    public function resetPassword(Request $request): JsonResponse
    {
        $data = $request->validate( [
            'email' => ['required', 'string', 'email', 'max:191'],
            'password' => ['required', 'string', 'min:8'],
            'code' => ['required', 'string', 'size:' . config('auth.reset_code_size')],
        ]);
        /** @var User $user */
        $user = User::where('email', $data['email'])->first();
        if ($user) {
            $pwdReset = ApiPasswordResets::where('user_id', $user->id)
                ->where('signature', make_signature([$user->email, $data['code']]))
                ->where('created_at', '>=', Carbon::now()->subSeconds(config('auth.reset_code_valid_period')))
                ->first();
            if ($pwdReset) {
                ApiPasswordResets::where('user_id', $user->id)->delete();
                $user->password = Hash::make($data['password']);
                $user->refresh_token_signature = null;
                $user->refresh_token_created_at = null;
                $user->save();
                $user->tokens()->delete();
                return $this->makeJsonResponse(null);
            }
        }
        throw new AuthenticationException();
    }

    /**
     * @throws AuthenticationException
     */
    public function refreshTokens(Request $request): JsonResponse
    {
        $data = $request->validate( [
            'refresh_token' => ['required', 'string', 'size:' . config('auth.refresh_token_size')],
        ]);
        /** @var User $user */
        $user = User::where('refresh_token_signature', make_signature([$data['refresh_token']]))
            ->where('refresh_token_created_at', '>=', Carbon::now()->subSeconds(config('auth.refresh_token_valid_period')))
            ->first();
        if ($user) {
            return $this->makeJsonResponse($user->regenerateTokens());
        }
        throw new AuthenticationException();
    }

    public function logout(Request $request): JsonResponse
    {
        $request->user()->clearAllTokens();
        return $this->makeJsonResponse();
    }
}
