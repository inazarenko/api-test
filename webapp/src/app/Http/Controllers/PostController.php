<?php

namespace App\Http\Controllers;

use App\Exceptions\ForbiddenException;
use App\Exceptions\ObjectNotFoundException;
use App\Models\Post;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class PostController extends APIController
{
    private const PAGE_SIZE = 10;
    private const POST_MODE_ADMIN = 'admin';
    private const POST_MODE_USER = 'user';

    /**
     * @throws ForbiddenException
     */
    public function index(Request $request): JsonResponse
    {
        $data = $request->validate([
            'page' => ['integer', 'min:1'],
            'size' => ['integer', 'min:1'],
            'mode' => ['string', Rule::in([self::POST_MODE_ADMIN, self::POST_MODE_USER])],
        ]);
        $mode = $data['mode'] ?? self::POST_MODE_USER;
        if ($mode === self::POST_MODE_ADMIN && !$request->user()->tokenCan('admin')) {
            throw new ForbiddenException();
        }
        $size = $data['size'] ?? self::PAGE_SIZE;
        $query = Post::orderBy('id', 'ASC');
        if ($mode === self::POST_MODE_USER) {
            $query = $query->where('is_published', true);
        }
        return $this->makeJsonResponse($query->paginate($size));
    }

    /**
     * @throws ObjectNotFoundException
     * @throws ForbiddenException
     */
    public function store(Request $request): JsonResponse
    {
        $data = $request->validate([
            'is_published' => ['required', 'integer', Rule::in([0, 1])],
            'author_id' => ['integer', 'min:1'],
            'caption' => ['required', 'string', 'max:191'],
            'body' => ['required', 'string', 'max:65535'],
        ]);
        if (!$request->user()->tokenCan('user')) {
            throw new ForbiddenException();
        }
        $userId = $data['author_id']??$request->user()->id;
        $user = User::whereId($userId)->first();
        if (!$user) {
            throw new ObjectNotFoundException('User not found');
        }
        $post = new Post();
        $post->author()->associate($user);
        foreach ($data as $key => $val) {
            if ($key !== 'author_id') {
                $post->{$key} = $val;
            }
        }
        $post->save();
        return $this->makeJsonResponse($post);
    }

    /**
     * @throws ObjectNotFoundException
     * @throws ForbiddenException
     */
    public function show(Request $request, int $id): JsonResponse
    {
        return $this->makeJsonResponse($this->getPostForEdit($request, $id));
    }

    /**
     * @throws ForbiddenException
     * @throws ObjectNotFoundException
     */
    private function getPostForEdit(Request $request, int $id): Post
    {
        $post = Post::whereId($id)->first();
        if (!$post) {
            throw new ObjectNotFoundException('Post not found');
        }
        if ($request->user()->id !== $post->author->id && !$request->user()->tokenCan('admin')) {
            throw new ForbiddenException();
        }
        return $post;
    }

    /**
     * @throws ForbiddenException
     * @throws ObjectNotFoundException
     */
    public function destroy(Request $request, int $id): JsonResponse
    {
        $this->getPostForEdit($request, $id)->delete();
        return $this->makeJsonResponse();
    }

    /**
     * @throws ForbiddenException
     * @throws ObjectNotFoundException
     */
    public function update(Request $request, int $id): JsonResponse
    {
        $post = $this->getPostForEdit($request, $id);
        $data = $request->validate([
            'is_published' => ['integer', Rule::in([0, 1])],
            'author_id' => ['integer', 'min:1'],
            'caption' => ['string', 'max:191'],
            'body' => ['string', 'max:65535'],
        ]);
        foreach ($data as $key => $val) {
            if ($key === 'author_id') {
                $user = User::whereId($val)->first();
                if (!$user) {
                    throw new ObjectNotFoundException('User not found');
                }
                $post->author()->associate($user);
            } else {
                $post->{$key} = $val;
            }
        }
        if (count($data) > 0) {
            $post->save();
        }
        return $this->makeJsonResponse($post);
    }

}
