<?php

namespace App\Http\Controllers;

use App\Exceptions\ForbiddenException;
use App\Exceptions\ObjectNotFoundException;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends APIController
{
    public const USER_LIST_PAGE_SIZE = 10;

    /**
     * @throws ForbiddenException
     */
    public function updateUser(Request $request, string $uid): JsonResponse
    {
        $currentUserId = $request->user()->id;
        $currentUser = $request->user()->id;
        $id = $uid === 'me' ? $currentUser : (int)$uid;
        $isAdmin = $request->user()->tokenCan('admin');
        if ($currentUserId != $id && !$isAdmin) {
            throw new ForbiddenException();
        }
        $data = $request->validate([
            'login' => ['string', 'max:191', 'unique:users,login,' . $id],
            'email' => ['string', 'email', 'max:191', 'unique:users,email,' . $id],
            'password' => ['string', 'min:8'],
            'abilities' => ['array'],
            'abilities.*' => ['string', 'min:1'],
        ]);
        if (isset($data['abilities']) && ($currentUserId == $id || !$isAdmin)) {
            unset($data['abilities']);
        }
        /** @var User $user */
        $user = User::whereId($id)->first();
        if (count($data) > 0) {
            if (isset($data['password'])) {
                $data['password'] = Hash::make($data['password']);
            }
            foreach ($data as $key => $value) {
                $user->{$key} = $value;
            }
            $user->save();
        }
        return $this->makeJsonResponse($user->regenerateTokens());
    }

    /**
     * @throws ForbiddenException
     */
    public function readUser(Request $request, string $id): JsonResponse
    {
        $currentUser = $request->user()->id;
        if ($id === 'me' || $currentUser === (int)$id) {
            return $this->makeJsonResponse(User::whereId($currentUser)->first());
        }

        if ($request->user()->tokenCan('admin')) {
            return $this->makeJsonResponse(User::whereId((int)$id)->first());
        }
        throw new ForbiddenException();
    }

    /**
     * @throws ForbiddenException
     */
    public function findUser(Request $request): JsonResponse
    {
        if (!$request->user()->tokenCan('admin')) {
            throw new ForbiddenException();
        }
        $data = $request->validate([
            'term' => ['required', 'string', 'min:1'],
            'page' => ['integer', 'min:1'],
            'size' => ['integer', 'min:1'],
        ]);
        $page = isset($data['page']) ? (int)$data['page'] : 1;
        $pageSize = isset($data['size']) ? (int)$data['size'] : self::USER_LIST_PAGE_SIZE;
        $list = User::where(is_numeric($data['term']) ? 'id' : 'login', 'like', "%{$data['term']}%")
            ->offset(($page - 1) * $pageSize)->limit($pageSize)->get();
        return $this->makeJsonResponse([
            'items' => $list,
            //The SQL_CALC_FOUND_ROWS query modifier and accompanying FOUND_ROWS() function
            //are deprecated as of MySQL 8.0.17;
            //This method of detecting end of list will require one more iteration
            //when total elements count can be divided by PAGE_SIZE without remainder
            //but it allows to eliminate one DB request
            'more' => count($list) >= $pageSize,
        ]);
    }

    /**
     * @throws ForbiddenException
     * @throws ObjectNotFoundException
     *
     * This function cleans all personal data. It allows to be complied with GDPR but without removing User object.
     */
    public function deleteUser(Request $request, string $id): JsonResponse
    {
        $currentUser = $request->user()->id;
        if ($id === 'me' || $currentUser === (int)$id || $request->user()->tokenCan('admin')) {
            $user = User::whereId($id === 'me' ? $currentUser : (int)$id)->first();
            if ($user) {
                $user->clearAllTokens();
                $user->login = null;
                $user->email = null;
                $user->password = null;
                $user->abilities = [];
                $user->created_at=Carbon::createFromTimestamp(1);
                $user->updated_at=Carbon::createFromTimestamp(1);
                $user->save();
                return $this->makeJsonResponse();
            } else {
                throw new ObjectNotFoundException('User not found');
            }
        }
        throw new ForbiddenException();
    }
}
