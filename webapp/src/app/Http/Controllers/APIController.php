<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;


class APIController extends Controller
{
    protected function makeJsonResponse($data = null, int $status = 200, string $message = '', int $code = 0, $debugData = null): JsonResponse
    {
        $result = [
            'error' => $status >= 400 || $status < 200,
            'message' => $message,
            'status' => $status,
            'code' => $code,
            'data' => $data
        ];
        if (config('app.debug')) {
            $result['debug'] = $debugData;
        }
        return response()->json($result, $status);
    }
}
