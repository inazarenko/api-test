<?php
if (! function_exists('make_signature')) {
    function make_signature(array $data): string {
        return hash('sha256', implode('|', $data));
    }
}
