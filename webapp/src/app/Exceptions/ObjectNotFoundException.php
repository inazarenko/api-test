<?php

namespace App\Exceptions;

use Exception;

class ObjectNotFoundException extends Exception
{
    public function __construct(string $msg = 'Object not found')
    {
        parent::__construct($msg, 404);
    }
}
