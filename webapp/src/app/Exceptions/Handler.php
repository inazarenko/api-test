<?php

namespace App\Exceptions;

use Illuminate\Auth\AuthenticationException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Illuminate\Validation\ValidationException;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        //
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
    }

    public function render($request, Throwable $exception)
    {
        return $this->handleApiException($request, $exception);
    }

    private function handleApiException($request, Throwable $exception)
    {
        $data = null;
        if (method_exists($exception, 'getMessage')) {
            $message = $exception->getMessage();
        } else {
            $message = 'Oops... Something went wrong';
        }
        if (method_exists($exception, 'getStatusCode')) {
            $statusCode = $exception->getStatusCode();
        } else {
            $statusCode = 500;
        }
        switch (true)
        {
            case $exception instanceof ValidationException:
                $statusCode = $exception->status;
                $data = $exception->errors();
                break;
            case $exception instanceof AuthenticationException:
                $statusCode = 401;
                break;
            case $exception instanceof ForbiddenException:
            case $exception instanceof ObjectNotFoundException:
                $statusCode = $exception->getCode();
                break;
            default:
                $data = get_class($exception);
                break;
        }

        $result = [
            'error' => true,
            'message' => $message,
            'status' => $statusCode,
            'code' => 0,
            'data' => $data
        ];
        if (config('app.debug')) {
            $result['debug'] = $exception->getTrace();
        }
        return response()->json($result, $statusCode);
    }
}
