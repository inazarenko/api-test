<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RefreshHistory extends Model
{
    protected $table = 'refresh_history';
    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'signature',
    ];

    public $timestamps = false;
}

