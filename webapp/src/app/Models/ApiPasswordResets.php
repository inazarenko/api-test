<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ApiPasswordResets extends Model
{
    public $timestamps = false;

    protected $table = 'api_passwords_reset';
    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'created_at' => 'datetime',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'user_id',
        'signature',
        'created_at',
    ];
}
