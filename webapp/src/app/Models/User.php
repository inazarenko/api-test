<?php

namespace App\Models;

use App\Http\Data\TokenHolder;
use App\Notifications\APIPasswordResetNotification;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\QueryException;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Carbon;
use Illuminate\Support\Str;
use Laravel\Sanctum\HasApiTokens;
use Laravel\Sanctum\NewAccessToken;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var string[]
     */
    protected $fillable = [
        'login',
        'email',
        'password',
        'abilities',
        'refresh_token_signature',
        'refresh_token_created_at',
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'refresh_token_signature',
        'refresh_token_created_at',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array
     */
    protected $casts = [
        'refresh_token_created_at' => 'datetime',
        'abilities' => 'array'
    ];

    /**
     * Create a new personal access token for the user.
     *
     * @param string $name
     * @param array $abilities
     * @return NewAccessToken
     */
    public function createToken(string $name, array $abilities = [], bool $preservePrevious = false)
    {
        if (!$preservePrevious) {
            $this->tokens()->delete();
        }
        $plainTextToken = Str::random(config('auth.access_token_size'));
        $token = $this->tokens()->create([
            'name' => $name,
            'token' => make_signature([$plainTextToken]),
            'abilities' => count($abilities) < 1 ? $this->abilities : $abilities,
        ]);
        return new NewAccessToken($token, $token->getKey() . '|' . $plainTextToken);
    }

    public function clearAllTokens(): void
    {
        $this->refresh_token_signature = null;
        $this->refresh_token_created_at = null;
        $this->save();
        $this->tokens()->delete();
        ApiPasswordResets::where('user_id', $this->id)->delete();
    }

    private function createRefreshToken(): string
    {
        $success = false;
        $code = '';
        while (!$success) {
            $code = Str::random(config('auth.refresh_token_size'));
            $signature = make_signature([$code]);
            try {
                RefreshHistory::create(['signature' => $signature])->save();
                $this->refresh_token_signature = $signature;
                $this->refresh_token_created_at = Carbon::now();
                $this->save();
                $success = true;
            } catch (QueryException $exception) {
                $success = $exception->errorInfo[1] != 1062;//1062- duplicate entry
            }
        }
        return $code;
    }

    public function routeNotificationForMail($notification)
    {
        return $this->email;
    }

    public function sendPasswordResetNotification($token)
    {
        $this->notify(new APIPasswordResetNotification($token));
    }

    public function regenerateTokens():TokenHolder
    {
        $this->clearAllTokens();
        $token = $this->createToken('access_token')->plainTextToken;
        return new TokenHolder($token, $this->createRefreshToken());
    }
}
